package de.uniba.wiai.dsg.ajp.assignment1.search.impl.util;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

import de.uniba.wiai.dsg.ajp.assignment1.search.SearchTask;

public final class SearchHelper
{
	private SearchHelper()
	{
	}

	/*
	 * Geht von Root-Folder ab immer tiefer durch den Directory-Baum. Für jeden
	 * gefundene Sub-Path wird validateDirs(...) aufgerufen
	 */
	public static void addValidDirsToList(final SearchTask task, List<Path> dirList)
	{

		try(Stream<Path> stream = Files.walk(Paths.get(task.getRootFolder())))
		{
			stream.forEach(path -> validateDirs(path, task.getIgnoreFile(), dirList));
		} catch(IOException exception)
		{

		}
	}

	/*
	 * Geht die dirList durch und ruft validateFiles(...) für jeden Path in der
	 * Liste auf.
	 */
	public static void addValidFilesToList(final SearchTask task, List<Path> dirList, List<Path> fileList)
	{
		dirList.forEach(path -> validateFiles(path, task, fileList));
	}

	/*
	 * Wird für jeden gefundene Path von addValidDirsToList aufgerufen. Schaut,
	 * ob der Path ein Directory ist und ob der Path-String den String enthält,
	 * der ignoriert werden soll. Wenn der Path valid ist, wird er der dirList
	 * hinzugefügt.
	 * 
	 * TODO: String ignreFile ist eigentlich eine File, die mehrere ignores
	 * enthält. Wir müssen also eigentlich eine File öffnen, jede Zeile lesen,
	 * die diese enthält und entscheiden ob die Input-File ignoriert werden
	 * soll.
	 */
	private static void validateDirs(Path path, String ignoreFile, List<Path> dirList)
	{

		if(Files.isDirectory(path) && path.toString().contains(ignoreFile) == false)
		{
			dirList.add(path);
		}
	}

	/*
	 * Wird für jedes Directory in dirList aufgerufen und schaut ob der Path
	 * Sub-Paths enthält, welche eine File sind. Schaut, ob die File mit der
	 * gewünschten Extension endet und fügt der fileList dann diese File hinzu.
	 */
	private static void validateFiles(Path path, final SearchTask task, List<Path> fileList)
	{
		try(DirectoryStream<Path> stream = Files.newDirectoryStream(path))
		{
			for(Path p : stream)
			{
				if(Files.isRegularFile(p) && p.toString().endsWith(task.getFileExtension()))
				{
					fileList.add(p);
				}
			}
		} catch(IOException e)
		{
		}
	}
}
