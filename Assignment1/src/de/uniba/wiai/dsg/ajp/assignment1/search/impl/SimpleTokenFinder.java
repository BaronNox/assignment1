package de.uniba.wiai.dsg.ajp.assignment1.search.impl;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import de.uniba.wiai.dsg.ajp.assignment1.search.SearchTask;
import de.uniba.wiai.dsg.ajp.assignment1.search.TokenFinder;
import de.uniba.wiai.dsg.ajp.assignment1.search.TokenFinderException;
import de.uniba.wiai.dsg.ajp.assignment1.search.impl.util.SearchHelper;

public class SimpleTokenFinder implements TokenFinder
{
	private List<Path> dirList;
	private List<Path> fileList;

	public SimpleTokenFinder()
	{
		dirList = new ArrayList<>();
		fileList = new ArrayList<>();

		/*
		 * DO NOT REMOVE
		 *
		 * REQUIRED FOR GRADING
		 */
	}

	@Override
	public void search(final SearchTask task) throws TokenFinderException
	{
		SearchHelper.addValidDirsToList(task, dirList);
		SearchHelper.addValidFilesToList(task, dirList, fileList);

		searchForToken(task);

		// Debug syso output von Directory Namen bzw. File Namen. par:
		// dirList/fileList
		// for (Path p : fileList)
		// System.out.println(p.toString());

	}

	private void searchForToken(final SearchTask task)
	{
		for(Path p : fileList)
		{
			// TODO: search each File in fileList for token
		}
	}
}
